/*
 * SPDX-License-Identifier: BSD-2-Clause
 * 
 * Copyright (c) 2024 Vincent DEFERT. All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions 
 * are met:
 * 
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright 
 * notice, this list of conditions and the following disclaimer in the 
 * documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS 
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE 
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; 
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER 
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
 * POSSIBILITY OF SUCH DAMAGE.
 */
#include "project-defs.h"

#define _TIMER_FREQ 8000ul
#define _BEEP_MS 75ul
#define _PAUSE_MS 75ul
#define _LONG_BEEP_MS 450ul
#define _WAIT_MS 15000ul

#define _LED_ON_MS 200ul
#define _LED_OFF_MS 800ul

#define _LED_ON_COUNT ((uint32_t) (_TIMER_FREQ * _LED_ON_MS / 1000ul))
#define _LED_OFF_COUNT ((uint32_t) (_TIMER_FREQ * _LED_OFF_MS / 1000ul))

#define _TIMER0_RELOAD_VALUE ((uint16_t) (65536ul - (MCU_FREQ / _TIMER_FREQ / 12ul)))

#ifdef GPIO_HAS_P5
	#define _BUZZER P5_4
#else
	#define _BUZZER P3_4
#endif

#define _LED P3_3

static uint32_t _steps[] = {
	(uint32_t) (_TIMER_FREQ * _WAIT_MS / 1000ul),
	(uint32_t) (_TIMER_FREQ * _BEEP_MS / 1000ul),
	(uint32_t) (_TIMER_FREQ * (_WAIT_MS - 1ul * _BEEP_MS - 0ul * _PAUSE_MS) / 1000ul),
	(uint32_t) (_TIMER_FREQ * _BEEP_MS / 1000ul),
	(uint32_t) (_TIMER_FREQ * _PAUSE_MS / 1000ul),
	(uint32_t) (_TIMER_FREQ * _BEEP_MS / 1000ul),
	(uint32_t) (_TIMER_FREQ * (_WAIT_MS - 2ul * _BEEP_MS - 1ul * _PAUSE_MS) / 1000ul),
	(uint32_t) (_TIMER_FREQ * _BEEP_MS / 1000ul),
	(uint32_t) (_TIMER_FREQ * _PAUSE_MS / 1000ul),
	(uint32_t) (_TIMER_FREQ * _BEEP_MS / 1000ul),
	(uint32_t) (_TIMER_FREQ * _PAUSE_MS / 1000ul),
	(uint32_t) (_TIMER_FREQ * _BEEP_MS / 1000ul),
	(uint32_t) (_TIMER_FREQ * (_WAIT_MS - 3ul * _BEEP_MS - 2ul * _PAUSE_MS) / 1000ul),
	(uint32_t) (_TIMER_FREQ * _BEEP_MS / 1000ul),
	(uint32_t) (_TIMER_FREQ * _PAUSE_MS / 1000ul),
	(uint32_t) (_TIMER_FREQ * _BEEP_MS / 1000ul),
	(uint32_t) (_TIMER_FREQ * _PAUSE_MS / 1000ul),
	(uint32_t) (_TIMER_FREQ * _BEEP_MS / 1000ul),
	(uint32_t) (_TIMER_FREQ * _PAUSE_MS / 1000ul),
	(uint32_t) (_TIMER_FREQ * _LONG_BEEP_MS / 1000ul),
};

#define _STEP_MAX (sizeof(_steps) / sizeof(_steps[0]))
#define _STEP_IDLE -1

// Set when the push button is pressed.
static __bit _buttonPressed;

static int8_t _currentStep;

static uint32_t _stepCounter;

static uint32_t _ledCounter;

INTERRUPT(timer0_isr, TIMER0_INTERRUPT) {
	if (_stepCounter) {
		_stepCounter--;
		
		// Even steps are silent, odd steps produce a beep.
		if (_currentStep & 1) {
			_BUZZER = !_BUZZER;
		}
	}
	
	if (_ledCounter) {
		_ledCounter--;
	}
}

INTERRUPT(extint3_isr, EXTINT3_INTERRUPT) {
	_buttonPressed = 1;
}

static void _turnOff() {
	// Turn everything off.
	T0RUN = 0;
	_LED = 1;
	_BUZZER = 1;
	
	// Prepare state for next wake up.
	_currentStep = _STEP_IDLE;
	_stepCounter = 0;
	_ledCounter = 0;
	
	// Enter power-down mode to improve battery life.
	// Next press on push button will wake the sytem up.
	PCON |= M_PD;
	NOP();
	NOP();
}

void main() {
	INIT_EXTENDED_SFR()

	// Configure GPIO --------------------------------------------------
	
#ifdef GPIO_HAS_P5
	// STC8
	// P5.5 is high-impedance, P5.4 is push-pull.
	P5M0 = 0x10;
	P5M1 = 0x20;
	// P3.3 is open-drain.
	P3M0 = 0x08;
	P3M1 = 0x08;
#else
	// STC15
	// P3.5 is high-impedance, P3.4 is push-pull,
	// P3.3 is open-drain.
	P3M0 = 0x18;
	P3M1 = 0x28;
#endif

	// Enable push button interrupt ------------------------------------
	_buttonPressed = 0;
	INT_CLKO |= M_INT3IE;
	
	// Configure T0 ----------------------------------------------------
	// Operate as timer in mode 0
	TMOD &= 0xf0;
	// Use 12x prescaler
	AUXR &= ~M_T0x12;
	// Enable timer interrupt
	T0IE = 1;

	// Enable interrupts -----------------------------------------------
	EA = 1;
	
	// Enter power-down mode until the push button is pressed.
	_turnOff();
	
	// Main loop -------------------------------------------------------
	while (1) {
		if (_buttonPressed) {
			_buttonPressed = 0;
			
			if (_currentStep == _STEP_IDLE) {
				// Start tea timer
				T0 = _TIMER0_RELOAD_VALUE;
				T0RUN = 1;
			} else {
				// Stop tea timer
				_turnOff();
			}
		} else {
			if (_stepCounter == 0) {
				_currentStep++;
				
				if (_currentStep == _STEP_MAX) {
					_turnOff();
				} else {
					_stepCounter = _steps[_currentStep];
				}
			}
			
			if (_ledCounter == 0) {
				_LED = !_LED;
				_ledCounter = _LED ? _LED_ON_COUNT : _LED_OFF_COUNT;
			}
		}
	}
}
